
package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {

 @Autowired
 private PetRepository pets;
 private Pet pet;

 private static final int TEST_PET_ID = 2;
 private static final int TEST_OWNER_ID = 1;
 private static final int TEST_VISIT_ID = 1;
 private static final int TEST_VET_ID = 1;
 
 	@Before
 	public void init() {
	  		
		if (this.pet==null) {
			pet = new Pet();
			pet.setId(TEST_PET_ID);
			pet.setName("Garfield");
			pet.setComments("It roars");
			
			Owner george = new Owner();
	        george.setId(TEST_OWNER_ID);
	        george.setFirstName("George");
	        george.setLastName("Franklin");
	        george.setAddress("110 W. Liberty St.");
	        george.setCity("Madison");
	        george.setTelephone("6085551023");
	        
	        pet.setOwner(george);
	        
	        PetType type = new PetType();
	        type.setId(1);
	        type.setName("Caucasico");
	        pet.setType(type);
			
			this.pets.save(pet);
		};
	 
 	}
 	
 	@Test
	public void testFindByName() {
		
		Collection<Pet> petFindByName = this.pets.findsPetsByName("Garfield");
		List<Pet> petlist = new ArrayList(petFindByName);
				
		assertEquals(petFindByName.size(), 1);
		
		assertNotNull(petlist.get(0));
		assertEquals(petlist.get(0).getName(), "Garfield");	
		assertEquals(petlist.get(0).getComments(), "It roars");	
		assertEquals(petlist.get(0).getId(), new Integer(2));	
	}
 
 	@Test
 	public void testdelete() {
		this.pets.delete(this.pet);
		assertNull(pets.findById(this.pet.getId()));
 	}
 

  
 }
 
