package org.springframework.samples.petclinic.visit;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTests {
	
	
		
	@Autowired
	private VisitRepository visits;
	
    @MockBean
    private VetRepository vets;
    
	@MockBean
	private PetRepository pets;
	  
	private Visit visit;
	private static final int TEST_VISIT_ID = 1;
	private static final int TEST_VET_ID = 1;
	private static final int TEST_PET_ID= 1;
	 
	@Before
	public void init() {
	
		
		if (this.visit==null) {
			visit = new Visit ();
			visit.setDescription("It is ok");
			
			String str = "2015-03-15";
		    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		    LocalDate dateTime = LocalDate.parse(str, formatter);
			visit.setDate(dateTime);
			
				Vet vetSave = new Vet();
				vetSave.setFirstName("Raul");
				vetSave.setId(TEST_VET_ID);
				vetSave.setLastName("Telo");
				vetSave.setHomeVisits(true);
				 given(this.vets.findById(TEST_VET_ID)).willReturn(vetSave);
				 
				 
			visit.setVet(vetSave);
			visit.setId(TEST_VISIT_ID);
			
				Pet garfield = new Pet();
			    garfield.setId(TEST_PET_ID);
			    given(this.pets.findById(TEST_PET_ID)).willReturn(new Pet());
			    
			visit.setPetId(TEST_PET_ID);
		
		
			this.visits.save(visit);
		}
	}
	
	@Test
	public void testfindById(){
		Visit visitFindById = this.visits.findById(visit.getId());
		
		assertNotNull(visitFindById.getDescription());
		assertEquals(visitFindById.getDescription(), visit.getDescription());	
		
		assertNotNull(visitFindById.getDate());
		assertEquals(visitFindById.getDate(), visit.getDate());	
		
		assertEquals(visitFindById.getId(), visit.getId());
		
		assertEquals(visitFindById.getPetId(), visit.getPetId());
		assertEquals(visitFindById.getVet().getId(),visit.getVet().getId());
		
	
	}
}