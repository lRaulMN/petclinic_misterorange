package org.springframework.samples.petclinic.owner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.VisitController;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Test class for {@link VisitController}
 *
 * @author Colin But
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VisitController.class)
public class VisitControllerTests {

    private static final int TEST_PET_ID = 1;
    private static final int TEST_OWNER_ID = 1;
    private static final int TEST_VISIT_ID = 1;
    private static final int TEST_VET_ID = 1;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VisitRepository visits;

    @MockBean
    private PetRepository pets;
    
    @MockBean
    private VetRepository vets;
    
    @MockBean
    private OwnerRepository owners;

	

   	@Test
	public void TestGetHomeVisits() {
		Vet vet = new Vet();
		vet.setHomeVisits(false);
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());

	}

	public void TestSetHomeVisits() {
		Vet vet = new Vet();
		vet.setHomeVisits(false);
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());

	}
    
    @Test 
    public void testVisitController() {
    	VisitController controller1 = new VisitController(visits, pets, vets);
    	VisitController controller2 = new VisitController(visits, pets, vets);
    
		assertThat(controller1).isNotEqualTo(controller2);
    }
    
    @Test 
    public void testPopulateVets() throws Exception {
    }
  
    @Before
    public void init() {
    	
    	Pet garfield = new Pet();
    	garfield.setId(TEST_PET_ID);
    	given(this.pets.findById(TEST_PET_ID)).willReturn(new Pet());
    
    	Owner george = new Owner();
    	george.setId(TEST_OWNER_ID);
    	george.setFirstName("George");
    	george.setLastName("Franklin");
    	george.setAddress("110 W. Liberty St.");
    	george.setCity("Madison");
    	george.setTelephone("6085551023");
    	given(this.owners.findById(TEST_OWNER_ID)).willReturn(george);
    
    	Vet vetTest = new Vet();
    	vetTest.setId(TEST_VET_ID);
    	vetTest.setFirstName("Raul");
    	vetTest.setLastName("Telo");
    	vetTest.setHomeVisits(true);
    	given(this.vets.findById(TEST_VET_ID)).willReturn(vetTest);
    
    	Visit visitTest = new Visit();
    	visitTest.setId(TEST_VISIT_ID);
    	visitTest.setDescription("It is ill");
    	visitTest.setVet(vetTest);
    	given(this.visits.findById(TEST_VISIT_ID)).willReturn(visitTest);
    }
    
    @Test
    public void testprocessEditVisitFromSuccess() throws Exception {
        mockMvc.perform(post("/owners/{ownerId}/pets/{petId}/visits/{visitId}/edit", TEST_OWNER_ID,TEST_PET_ID,TEST_VISIT_ID)
        		.param("description", "It is ok")
        )
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/owners/{ownerId}"));
    }

    @Test
    public void testInitNewVisitForm() throws Exception {
        mockMvc.perform(get("/owners/*/pets/{petId}/visits/new", TEST_PET_ID))
            .andExpect(status().isOk())
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }

    @Test
    public void testProcessNewVisitFormSuccess() throws Exception {
        mockMvc.perform(post("/owners/*/pets/{petId}/visits/new", TEST_PET_ID)
            .param("name", "George")
            .param("description", "Visit Description")
        )
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/owners/{ownerId}"));
    }

    @Test
    public void testProcessNewVisitFormHasErrors() throws Exception {
        mockMvc.perform(post("/owners/*/pets/{petId}/visits/new", TEST_PET_ID)
            .param("name", "George")
        )
            .andExpect(model().attributeHasErrors("visit"))
            .andExpect(status().isOk())
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }

}
