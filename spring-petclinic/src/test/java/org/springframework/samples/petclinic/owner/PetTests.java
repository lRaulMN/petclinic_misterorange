package org.springframework.samples.petclinic.owner;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.BeforeClass;
import org.junit.Test;

public class PetTests {
	
	Pet pet1 = new Pet();
	Pet pet2 = new Pet();
	
	
	@Test
	public void testSet() {
	
		pet2 = pet1;
		assertThat(pet1).isEqualTo(pet2);
		pet1.setWeight(10);
		assertThat(pet1.getWeight()).isNotEqualTo(20);
		assertThat(pet1.getWeight()).isEqualTo(10);
	}
	
	@Test
	public void testGet() {
		pet1 = new Pet();
		float comparate = 10;
		pet1.setWeight(comparate);
		assertThat(pet1.getWeight()).isEqualTo(comparate);
	}
	
	@Test
	public void testSetComment() {
		pet1 = new Pet();
		
		pet1.setComments("I love its little paw");
		assertThat(pet1.getComments()).isNotEqualTo("I don't like it");
		assertThat(pet1.getComments()).isEqualTo("I love its little paw");
	}
	
	@Test
	public void testGetComment() {
		pet1 = new Pet();
		pet2 = new Pet();
		
		pet1.setComments("I love its little paw");
		pet2.setComments("I hate his tail");
		assertThat(pet1.getComments()).isNotEqualTo("I don't like it");
		assertThat(pet1.getComments()).isEqualTo("I love its little paw");
		assertThat(pet1.getComments()).isNotEqualTo(pet2.getComments());
	}
}
