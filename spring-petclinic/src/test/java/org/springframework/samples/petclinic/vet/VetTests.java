/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author Dave Syer
 *
 */
public class VetTests {

    @Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }

	@Test
	public void TestGetHomeVisits() {
		Vet vet = new Vet();
		vet.setHomeVisits(false);
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());

	}

	@Test
	public void TestSetHomeVisits() {
		Vet vet = new Vet();
		vet.setHomeVisits(false);
		assertFalse(vet.getHomeVisits());
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits());

	}

    @Test
    public void testSetVisitsInternal() {
        Vet vet = new Vet();
        HashSet<Visit>  visits = new HashSet<>();
        visits.add(new Visit());
        vet.setVisitsInternal(visits);
        assertThat(visits).isEqualTo(vet.getVisitsInternal());
    }
    
    @Test
    public void testGetVisitsInternal() {
        Vet vet = new Vet();
        HashSet<Visit>  visits = new HashSet<>();
        visits.add(new Visit());
        vet.setVisitsInternal(visits);
        assertThat(visits).isEqualTo(vet.getVisitsInternal());
        
        visits.add(new Visit());
        assertThat(visits).isEqualTo(vet.getVisitsInternal());
        assertThat(vet.getVisitsInternal()).isNotEqualTo(new HashSet<>());
    }
    
    @Test
    public void testAddVisit() {
        Vet vet = new Vet();
        HashSet<Visit>  visits = new HashSet<>();
        
        Visit v = new Visit();
        visits.add(v);
        vet.addVisit(v);
        
        assertEquals(vet.getVisit(), new ArrayList<Visit>(visits));
    }
      
    @Test
    public void testGetVisit() {
        Vet vet = new Vet();
        HashSet<Visit>  visits = new HashSet<>();
        
        Visit v = new Visit();
        visits.add(v);
        vet.addVisit(v);
        
        assertEquals(vet.getVisit(), new ArrayList<Visit>(visits));
        
        Visit v2 = new Visit();
        visits.add(v2);
        vet.addVisit(v2);

        assertEquals(vet.getVisit(), new ArrayList<Visit>(visits));
        assertThat(vet.getVisit()).isNotEqualTo(new ArrayList());
    }
    

}
