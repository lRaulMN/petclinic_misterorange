package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertThat;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class VisitTest {

	@Test
    public void testSetVisit() {
		Visit visit1 = new Visit();
		Visit visit2 = new Visit();
		
		// Changing Description attribute
		visit1.setDescription("new test description 1");
		assertThat(visit1.getDescription()).isNotEqualTo(visit2.getDescription());
		// Putting the same attribute
		visit2.setDescription("new test description 1");
		assertThat(visit1.getDescription()).isEqualTo(visit2.getDescription());
		
		// Changing PetID attribute
		visit1 = new Visit();
		visit2 = new Visit();
		visit1.setPetId(99);
		visit2.setPetId(12);
		assertThat(visit1.getPetId()).isNotEqualTo(visit2.getPetId());
		
		// Changing one attribute
		Vet vet1 = new Vet();
		visit1.setVet(vet1);
		Vet vet2 = new Vet();
		visit2.setVet(vet2);
		assertThat(visit1.getVet()).isNotEqualTo(visit2.getVet());
    }
	
	@Test
    public void testGetVisit() {
		Visit visit1 = new Visit();
		
		// Changing Description attribute
		visit1.setDescription("new test description 1");
		assertThat(visit1.getDescription()).isEqualTo("new test description 1");
		
		// Changing PetID attribute
		visit1.setPetId(99);
		assertThat(visit1.getPetId()).isEqualTo(99);
		
		// Changing one attribute
		Vet vet1 = new Vet();
		visit1.setVet(vet1);
		assertThat(visit1.getVet()).isEqualTo(vet1);
    }
	
}
