package org.springframework.samples.katalon;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NR2 {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    System.setProperty("webdriver.gecko.driver", "/Users/Guille/geckodriver");
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testNR2() throws Exception {
    driver.get("http://127.0.0.1:8080/vets.html");
    assertEquals("James Carter", driver.findElement(By.linkText("James Carter")).getText());
    assertEquals("none", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='James Carter'])[1]/following::span[1]")).getText());
    assertEquals("Helen Leary", driver.findElement(By.linkText("Helen Leary")).getText());
    assertEquals("radiology", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Helen Leary'])[1]/following::span[1]")).getText());
    assertEquals("Linda Douglas", driver.findElement(By.linkText("Linda Douglas")).getText());
    assertEquals("dentistry", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Linda Douglas'])[1]/following::span[1]")).getText());
    assertEquals("Rafael Ortega", driver.findElement(By.linkText("Rafael Ortega")).getText());
    assertEquals("surgery", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Rafael Ortega'])[1]/following::span[1]")).getText());
    assertEquals("Henry Stevens", driver.findElement(By.linkText("Henry Stevens")).getText());
    assertEquals("radiology", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Henry Stevens'])[1]/following::span[1]")).getText());
    assertEquals("Sharon Jenkins", driver.findElement(By.linkText("Sharon Jenkins")).getText());
    assertEquals("none", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Sharon Jenkins'])[1]/following::span[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
