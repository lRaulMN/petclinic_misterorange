package org.springframework.samples.katalon;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NR5 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.gecko.driver", "/home/juanc/Descargas/geckodriver-v0.23.0-linux32/geckodriver");
		driver = new FirefoxDriver();
		baseUrl = "https://www.katalon.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testNR5() throws Exception {
		driver.get("http://localhost:8080/");
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find pets'])[1]/following::span[2]")).click();
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='none'])[1]/following::td[1]")).click();
		assertEquals("No", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='none'])[1]/following::td[1]")).getText());
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='radiology'])[1]/following::td[1]")).click();
		assertEquals("No", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='radiology'])[1]/following::td[1]")).getText());
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='surgery'])[1]/following::td[1]")).click();
		assertEquals("No", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='surgery'])[1]/following::td[1]")).getText());
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='surgery'])[2]/following::td[1]")).click();
		assertEquals("Yes", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='surgery'])[2]/following::td[1]")).getText());
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='radiology'])[2]/following::td[1]")).click();
		assertEquals("Yes", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='radiology'])[2]/following::td[1]")).getText());
		driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='none'])[2]/following::td[1]")).click();
		assertEquals("No", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='none'])[2]/following::td[1]")).getText());
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
